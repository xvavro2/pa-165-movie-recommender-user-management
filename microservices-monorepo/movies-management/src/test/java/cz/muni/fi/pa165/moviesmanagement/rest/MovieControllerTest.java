package cz.muni.fi.pa165.moviesmanagement.rest;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.facade.MovieFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MovieControllerTest {

    @Mock
    private MovieFacade movieFacade;

    @InjectMocks
    private MovieController movieController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllMovies_allCorrect_returnsMovieDtoCollection() {
        // Arrange
        Collection<MovieDto> expectedMovies = getSampleMovieData();
        when(movieFacade.getAllMovies()).thenReturn(expectedMovies);

        // Act
        Collection<MovieDto> actualMovies = movieController.getAllMovies().getBody();

        // Assert
        assertEquals(expectedMovies, actualMovies);
    }

    @Test
    void getMovieById_allCorrect_returnsMovieDto() {
        // Arrange
        long id = 1L;
        MovieDto expectedMovie = new MovieDto();
        when(movieFacade.getMovieById(id)).thenReturn(Optional.of(expectedMovie));

        // Act
        ResponseEntity<MovieDto> response = movieController.getMovieById(id);

        // Assert
        assertEquals(expectedMovie, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getMovieById_notFoundId_returnsNotFoundStatus() {
        // Arrange
        long id = 1L;
        when(movieFacade.getMovieById(id)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<MovieDto> response = movieController.getMovieById(id);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void getMovieByName_allCorrect_returnsOkStatus() {
        // Arrange
        String name = "Test Movie";
        MovieDto expectedMovie = new MovieDto();
        when(movieFacade.getMovieByName(name)).thenReturn(Optional.of(expectedMovie));

        // Act
        ResponseEntity<MovieDto> response = movieController.getMovieByName(name);

        // Assert
        assertEquals(expectedMovie, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getMovieByName_notFoundName_returnsNotFoundStatus() {
        // Arrange
        String name = "Non-existent Movie";
        when(movieFacade.getMovieByName(name)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<MovieDto> response = movieController.getMovieByName(name);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void getMoviesByGenre_allCorrect_returnsMovieDtoCollection() {
        // Arrange
        MovieGenre genre = MovieGenre.ACTION;
        Collection<MovieDto> expectedMovies = new ArrayList<>();
        when(movieFacade.getMoviesByGenre(genre)).thenReturn(expectedMovies);

        // Act
        Collection<MovieDto> actualMovies = movieController.getMoviesByGenre(genre).getBody();

        // Assert
        assertEquals(expectedMovies, actualMovies);
    }

    @Test
    void getGenres_allCorrect_returnsMovieGenresCollection() {
        // Arrange
        Collection<MovieGenre> expectedGenres = new ArrayList<>();
        when(movieFacade.getPossibleGenres()).thenReturn(expectedGenres);

        // Act
        Collection<MovieGenre> actualGenres = movieController.getGenres().getBody();

        // Assert
        assertEquals(expectedGenres, actualGenres);
    }

    @Test
    void addMovie_allCorrect_returnMovieDto() {
        // Arrange
        CreateMovieDto createMovieDto = new CreateMovieDto(
                "Test Movie",
                "Test Director",
                "Test Description",
                Collections.singletonList(MovieGenre.ACTION),
                "testImageUrl",
                Collections.singletonList("Test Actor")
        );
        MovieDto addedMovie = new MovieDto();
        when(movieFacade.addMovie(createMovieDto)).thenReturn(addedMovie);

        // Act
        MovieDto result = movieController.addMovie(createMovieDto).getBody();

        // Assert
        assertEquals(addedMovie, result);
    }

    @Test
    void updateMovie_allCorrect_returnsNewMovieDto() {
        // Arrange
        MovieDto movieDto = new MovieDto();
        MovieDto updatedMovie = new MovieDto();
        when(movieFacade.updateMovie(movieDto)).thenReturn(updatedMovie);

        // Act
        MovieDto result = movieController.updateMovie(movieDto).getBody();

        // Assert
        assertEquals(updatedMovie, result);
    }

    @Test
    void deleteMovie_allCorrect_returnsOk() {
        // Arrange
        long id = 1L;
        when(movieFacade.deleteMovie(id)).thenReturn(true);

        // Act
        ResponseEntity<Boolean> response = movieController.deleteMovie(id);

        // Assert
        assertEquals(Boolean.TRUE, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void deleteMovie_notFoundId_returnsNotFoundStatus() {
        // Arrange
        long id = 1L;
        when(movieFacade.deleteMovie(id)).thenReturn(false);

        // Act
        ResponseEntity<Boolean> response = movieController.deleteMovie(id);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    public Collection<MovieDto> getSampleMovieData() {
        Collection<MovieDto> movies = new ArrayList<>();
        movies.add(new MovieDto());
        movies.add(new MovieDto());
        movies.add(new MovieDto());
        return movies;
    }

}
