package cz.muni.fi.pa165.moviesmanagement.facade;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.mapper.MovieMapper;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.service.MovieService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class MovieFacadeImplTest {

    @Mock
    private MovieService movieService;

    @Mock
    private MovieMapper movieMapper;

    @InjectMocks
    private MovieFacadeImpl movieFacade;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllMovies_allCorrect_returnsListOfMovies() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        when(movieService.getAll()).thenReturn(Collections.singletonList(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Collection<MovieDto> result = movieFacade.getAllMovies();

        // Assert
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.contains(movieDto));
    }

    @Test
    public void getMovieById_allCorrect_returnsMovieDto() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        Long id = 1L;
        when(movieService.findById(id)).thenReturn(Optional.of(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Optional<MovieDto> result = movieFacade.getMovieById(id);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(movieDto, result.get());
    }

    @Test
    public void getMovieById_notFound_returnsEmptyResult() {
        // Arrange
        Long id = 1L;
        when(movieService.findById(id)).thenReturn(Optional.empty());

        // Act
        Optional<MovieDto> result = movieFacade.getMovieById(id);

        // Assert
        assertFalse(result.isPresent());
    }

    @Test
    public void getMoviesByGenre_allCorrect_returnsMovieDto() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        MovieGenre genre = MovieGenre.ACTION;
        when(movieService.findByGenre(genre)).thenReturn(Collections.singletonList(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Collection<MovieDto> result = movieFacade.getMoviesByGenre(genre);

        // Assert
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.contains(movieDto));
    }

    @Test
    public void getMovieByName_allCorrect_returnsMovieDzo() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        String name = "Test Movie";
        when(movieService.findByName(name)).thenReturn(Optional.of(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Optional<MovieDto> result = movieFacade.getMovieByName(name);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(movieDto, result.get());
    }

    @Test
    public void getMovieByName_notFoundName_returnsEmpty() {
        // Arrange
        String name = "Test Movie";
        when(movieService.findByName(name)).thenReturn(Optional.empty());

        // Act
        Optional<MovieDto> result = movieFacade.getMovieByName(name);

        // Assert
        assertFalse(result.isPresent());
    }


    @Test
    public void getPossibleGenres_AllCorrect_returnsGenresCollection() {
        // Arrange
        Collection<MovieGenre> genres = Arrays.asList(MovieGenre.values());
        when(movieService.getPossibleGenres()).thenReturn(genres);

        // Act
        Collection<MovieGenre> result = movieFacade.getPossibleGenres();

        // Assert
        assertFalse(result.isEmpty());
        assertEquals(genres.size(), result.size());
        assertTrue(result.containsAll(genres));
    }

    @Test
    public void addMovie_allCorrect_returnsMovieDto() {
        // Arrange
        CreateMovieDto createMovieDto = getSampleCreateMovieDto();
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        when(movieMapper.convertToEntity(createMovieDto)).thenReturn(movie);
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        MovieDto result = movieFacade.addMovie(createMovieDto);

        // Assert
        assertNotNull(result);
        assertEquals(movieDto, result);
    }

    // Test pro updateMovie metodu
    @Test
    public void updateMovie_allCorrect_returnsMovieDto() {
        // Arrange
        MovieDto movieDto = new MovieDto();
        Movie movie = new Movie();
        when(movieMapper.convertToEntity(movieDto)).thenReturn(movie);
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        MovieDto result = movieFacade.updateMovie(movieDto);

        // Assert
        assertNotNull(result);
        assertEquals(movieDto, result);
    }

    // Test pro deleteMovie metodu
    @Test
    public void deleteMovie_allCorrect_returnsTrue() {
        // Arrange
        Long id = 1L;
        Movie movie = new Movie();
        when(movieService.findById(id)).thenReturn(Optional.of(movie));

        // Act
        Boolean result = movieFacade.deleteMovie(id);

        // Assert
        assertTrue(result);
        verify(movieService, times(1)).delete(movie);
    }

    // Test pro deleteMovie metodu, když není nalezen žádný film
    @Test
    public void deleteMovie_notFoundId_returnsFalse() {
        // Arrange
        Long id = 1L;
        when(movieService.findById(id)).thenReturn(Optional.empty());

        // Act
        Boolean result = movieFacade.deleteMovie(id);

        // Assert
        assertFalse(result);
        verify(movieService, never()).delete(any());
    }

    public CreateMovieDto getSampleCreateMovieDto() {
        return new CreateMovieDto(
                "Test Movie",
                "Test Director",
                "description",
                Collections.singletonList(MovieGenre.ACTION),
                "testImageUrl",
                Collections.singletonList("Test Actor")
        );
    }
}