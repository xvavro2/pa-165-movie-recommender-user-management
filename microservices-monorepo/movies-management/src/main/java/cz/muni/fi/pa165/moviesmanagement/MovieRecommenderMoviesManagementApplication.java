package cz.muni.fi.pa165.moviesmanagement;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings({"PMD", "checkstyle:hideutilityclassconstructor"})  // spring triggers linters false-positives
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Movie Recommender Movies Management",
		version = "1.0", description = "Movies management API",
		contact = @io.swagger.v3.oas.annotations.info.Contact(name = "Mikulas Heinz", email = "483813@muni.cz")
))
public class MovieRecommenderMoviesManagementApplication {

	/**
	 * Main method to run the application.
	 * @param args command line arguments
	 */
	public static void main(final String[] args) {
		SpringApplication.run(MovieRecommenderMoviesManagementApplication.class, args);
	}

}
