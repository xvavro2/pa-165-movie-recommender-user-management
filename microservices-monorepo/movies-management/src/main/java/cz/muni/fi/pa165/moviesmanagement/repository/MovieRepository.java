package cz.muni.fi.pa165.moviesmanagement.repository;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.Optional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieRepository {

    private final Map<Long, Movie> movies = new HashMap<>();
    private Long maxId = 0L;

    /**
     * Retrieves all movies.
     *
     * @return A collection of all movies.
     */
    public Collection<Movie> getAll() {
        return movies.values();
    }

    /**
     * Retrieves a movie by its identifier.
     *
     * @param id The identifier of the movie.
     * @return An Optional containing the movie if found, or an empty Optional otherwise.
     */
    public Optional<Movie> findById(final Long id) {
        return Optional.ofNullable(movies.get(id));
    }

    /**
     * Retrieves a movie by its name.
     *
     * @param name The name of the movie.
     * @return An Optional containing the movie if found, or an empty Optional otherwise.
     */
    public Optional<Movie> findByName(final String name) {
        for (Movie movie : movies.values()) {
            if (movie.getName().equals(name)) {
                return Optional.of(movie);
            }
        }
        return Optional.empty();
    }

    /**
     * Retrieves all movies of a given genre.
     *
     * @param genre The genre to filter the movies.
     * @return A collection of movies of the specified genre.
     */
    public Collection<Movie> findByGenre(final MovieGenre genre) {
        Collection<Movie> result = new ArrayList<>();
        for (Movie movie : movies.values()) {
            if (movie.getGenres().contains(genre)) {
                result.add(movie);
            }
        }
        return result;
    }

    /**
     * Retrieves all possible movie genres.
     *
     * @return A collection of all possible movie genres.
     */
    public Collection<MovieGenre> getPossibleGenres() {
        return List.of(MovieGenre.values());
    }

    /**
     * Adds a new movie to the repository.
     *
     * @param movie The movie to be added.
     */
    public void add(final Movie movie) {
        movie.setId(maxId++);
        movies.put(movie.getId(), movie);
    }

    /**
     * Deletes a movie by its identifier.
     *
     * @param movie The movie to be deleted.
     */
    public void delete(final Movie movie) {
        movies.remove(movie.getId());
    }

    /**
     * Updates a movie in the repository.
     *
     * @param movie The movie to be updated.
     */
    public void update(final Movie movie) {
        movies.put(movie.getId(), movie);
    }
}
