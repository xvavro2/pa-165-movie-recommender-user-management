package cz.muni.fi.pa165.ratingsandreviews.service;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.repository.ReviewRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of ReviewService interface.
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    /**
     * The review repository.
     */
    private final ReviewRepository reviewRepository;

    /**
     * Constructor for ReviewServiceImpl.
     *
     * @param repository The review repository.
     */
    public ReviewServiceImpl(final ReviewRepository repository) {
        this.reviewRepository = repository;
    }

    /**
     * Retrieves all reviews.
     *
     * @return List of all reviews.
     */
    @Override
    public List<Review> getAllReviews() {
        return reviewRepository.findAll();
    }

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review.
     */
    @Override
    public Review addReview(final Review newReview) {
        reviewRepository.save(newReview);
        return newReview;
    }

    /**
     * Deletes a review by ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review.
     * @throws EntityNotFoundException if the review with the given ID does not exist.
     */
    @Override
    public Boolean deleteReview(final Long reviewId) {
        Optional<Review> review = reviewRepository.findById(reviewId);
        if (review.isPresent()) {
            reviewRepository.delete(review.get());
            return true;
        } else {
            throw new EntityNotFoundException("Review with id " + reviewId + "does not exist");
        }
    }

    /**
     * Retrieves a review by ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review.
     * @throws EntityNotFoundException if the review with the given ID does not exist.
     */
    @Override
    public Optional<Review> getReview(final Long reviewId) {
        return reviewRepository.findById(reviewId);
    }

    /**
     * Updates a review.
     *
     * @param updatedReview The updated review.
     * @return The updated review.
     */
    @Override
    @Transactional
    public Review updateReview(final Review updatedReview) {
        return reviewRepository.save(updatedReview);
    }
}
