package cz.muni.fi.pa165.ratingsandreviews.mapper;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.springframework.stereotype.Component;
/**
 * Mapper class responsible for mapping between ReviewDTO and Review entities.
 */

@Component
public class ReviewMapper {
    /**
     * Maps a ReviewDTO to a Review entity.
     *
     * @param reviewDto The ReviewDTO to be mapped.
     * @return The mapped Review entity.
     */
    public Review dtoToEntity(final ReviewDTO reviewDto) {
        Review review = new Review();
        review.setId(reviewDto.getId());
        review.setRatings(reviewDto.getRatings());
        review.setMovieId(reviewDto.getMovieId());
        review.setUserId(reviewDto.getUserId());
        review.setOverallRating(reviewDto.getOverallRating());
        return review;
    }
    /**
     * Maps a Review entity to a ReviewDTO.
     *
     * @param review The Review entity to be mapped.
     * @return The mapped ReviewDTO.
     */
    public ReviewDTO entityToDto(final Review review) {
        ReviewDTO reviewDto = new ReviewDTO();
        reviewDto.setId(review.getId());
        reviewDto.setRatings(review.getRatings());
        reviewDto.setMovieId(review.getMovieId());
        reviewDto.setUserId(review.getUserId());
        reviewDto.setOverallRating(review.getOverallRating());
        return reviewDto;
    }
}

