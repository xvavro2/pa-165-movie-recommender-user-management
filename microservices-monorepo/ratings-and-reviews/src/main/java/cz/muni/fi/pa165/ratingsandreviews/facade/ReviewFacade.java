package cz.muni.fi.pa165.ratingsandreviews.facade;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public interface ReviewFacade {

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review DTO.
     */
    ReviewDTO addReview(ReviewDTO newReview);

    /**
     * Deletes a review by its ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review DTO.
     */
    Boolean deleteReview(Long reviewId);

    /**
     * Updates a review.
     *
     * @param updatedReview The updated review.
     * @return The updated review DTO.
     */
    ReviewDTO updateReview(ReviewDTO updatedReview);

    /**
     * Retrieves a review by its ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review DTO.
     */
    Optional<ReviewDTO> getReview(Long reviewId);

    /**
     * Retrieves reviews by user ID.
     *
     * @param userId The ID of the user whose reviews are to be retrieved.
     * @return List of review DTOs.
     */
    List<ReviewDTO> getReviewsByUserId(Long userId);

    /**
     * Retrieves reviews sorted by value of its property rating.
     *
     * @param property The property on which sort wil be done
     * @param descending Boolean value -> true if sort is descending
     * @return List of sorted reviews
     */
    List<ReviewDTO> getAllReviewsSortedByProperty(FilmProperty property, Boolean descending);

    /**
     * Retrieves reviews sorted by value of its overall rating.
     *
     * @param descending Boolean value -> true if sort is descending
     * @return List of sorted reviews
     */
    List<ReviewDTO> getAllReviewsSortedByRating(Boolean descending);


}
