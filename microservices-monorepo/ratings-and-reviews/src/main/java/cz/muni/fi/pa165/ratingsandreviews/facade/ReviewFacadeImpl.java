package cz.muni.fi.pa165.ratingsandreviews.facade;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.mapper.ReviewMapper;
import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.service.ReviewService;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of ReviewFacade.
 */
@Service
public class ReviewFacadeImpl implements ReviewFacade {

    /**
     * The ReviewMapper instance.
     */
    private final ReviewMapper reviewMapper;

    /**
     * The ReviewService instance.
     */
    private final ReviewService reviewService;

    /**
     * Constructs ReviewFacadeImpl.
     *
     * @param service review service
     * @param mapper  the review mapper
     */
    public ReviewFacadeImpl(final ReviewMapper mapper,
                            final ReviewService service) {
        this.reviewService = service;
        this.reviewMapper = mapper;
    }

    /**
     * Adds a new review.
     *
     * @param newReview the review to add
     * @return the added review DTO
     */
    @Override
    public ReviewDTO addReview(final ReviewDTO newReview) {
        reviewService.addReview(reviewMapper.dtoToEntity(newReview));
        return newReview;
    }

    /**
     * Deletes a review by ID.
     *
     * @param reviewId the ID of the review to delete
     * @return the deleted review DTO
     */
    @Override
    @Transactional
    public Boolean deleteReview(final Long reviewId) {
        return reviewService.deleteReview(reviewId);
    }

    /**
     * Updates an existing review.
     *
     * @param updatedReview the updated review
     * @return the updated review DTO
     */
    @Override
    @Transactional
    public ReviewDTO updateReview(final ReviewDTO updatedReview) {
        reviewService.addReview(reviewMapper.dtoToEntity(updatedReview));
        return updatedReview;
    }

    /**
     * Retrieves a review by ID.
     *
     * @param reviewId the ID of the review to retrieve
     * @return the retrieved review DTO
     */
    @Override
    @Transactional
    public Optional<ReviewDTO> getReview(final Long reviewId) {
        Optional<Review> reviewDTO = reviewService.getReview(reviewId);
        return reviewDTO.map(reviewMapper::entityToDto);
    }

    /**
     * Retrieves reviews by user ID.
     *
     * @param userId the ID of the user
     * @return list of review DTOs by the user
     */
    @Override
    @Transactional
    public List<ReviewDTO> getReviewsByUserId(final Long userId) {
        return reviewService.getAllReviews().stream()
                .filter(review -> userId.equals(review.getUserId()))
                .map(reviewMapper::entityToDto)
                .collect(Collectors.toList());
    }

    /**
     * Retrieves all reviews sorted by overall rating.
     * @param descending Boolean value -> true if sort is descending
     * @return list of review DTOs sorted by overall rating
     */
    @Override
    @Transactional
    public List<ReviewDTO> getAllReviewsSortedByRating(final Boolean descending) {
        Comparator<Review> comparator = Comparator.comparing(Review::getOverallRating);

        return reviewService.getAllReviews().stream()
                .sorted(descending ? comparator.reversed() : comparator)
                .map(reviewMapper::entityToDto)
                .collect(Collectors.toList());
    }
    /**
     * Retrieves all reviews sorted by the specified property.
     * @param property The property on which sort will be done
     * @param descending Boolean value -> true if sort is descending
     * @return list of review DTOs sorted by the specified property
     */
    @Override
    @Transactional
    public List<ReviewDTO> getAllReviewsSortedByProperty(final FilmProperty property, final Boolean descending) {
        Comparator<Review> comparator = Comparator.comparing(review -> review.getRatings()
                .getOrDefault(property, 0.0f));

        return reviewService.getAllReviews().stream()
                .sorted(descending ? comparator.reversed() : comparator)
                .map(reviewMapper::entityToDto)
                .collect(Collectors.toList());
    }

}
