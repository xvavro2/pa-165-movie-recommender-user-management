package cz.muni.fi.pa165.ratingsandreviews.controller;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.facade.ReviewFacade;
import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;

@ExtendWith(MockitoExtension.class)
class ReviewControllerTest {

    @Mock
    private ReviewFacade reviewFacade;

    @InjectMocks
    private ReviewController reviewController;

    @Test
    void addReview_validReview_reviewAdded() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        ReviewDTO addedReview = new ReviewDTO();
        Mockito.when(reviewFacade.addReview(review)).thenReturn(addedReview);

        // Act
        ReviewDTO result = reviewController.addReview(review).getBody();

        // Assert
        Assertions.assertEquals(addedReview, result);
    }

    @Test
    void addReview_invalidReview_reviewNotAdded() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.addReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.addReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void deleteReview_validId_reviewDeleted() {
        // Arrange
        Mockito.when(reviewFacade.deleteReview(1L)).thenReturn(true);

        // Act
        Long result = reviewController.deleteReview(1L).getBody();

        // Assert
        Assertions.assertEquals(1L, result);
    }

    @Test
    void deleteReview_invalidId_reviewNotDeleted() {
        // Arrange
        Mockito.when(reviewFacade.deleteReview(1L)).thenReturn(false);

        // Act
        Long result = reviewController.deleteReview(1L).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void updateReview_validUpdateForExistingMovie_reviewUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        ReviewDTO updatedReview = new ReviewDTO();
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(updatedReview);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertEquals(updatedReview, result);
    }

    @Test
    void updateReview_invalidUpdateForExistingMovie_reviewNotUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void updateReview_validUpdateForNonExistingMovie_reviewNotUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        review.setId(1L);
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void updateReview_invalidUpdateForNonExistingMovie_reviewNotUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void getReview_validId_returnsReview() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.getReview(1L)).thenReturn(Optional.of(review));

        // Act
        ReviewDTO result = reviewController.getReview(1L).getBody();

        // Assert
        Assertions.assertEquals(review, result);
    }

    @Test
    void getReview_invalidId_returnsNull() {
        // Arrange
        Mockito.when(reviewFacade.getReview(1L)).thenReturn(Optional.empty());

        // Act
        ReviewDTO result = reviewController.getReview(1L).getBody();

        // Assert
        Assertions.assertNull(result);
    }


    @Test
    void getReviewsByUserId_existingUser_returnsReviews() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.getReviewsByUserId(1L)).thenReturn(java.util.Collections.singletonList(review));

        // Act
        List<ReviewDTO> resultBody = reviewController.getReviewsByUserId(1L).getBody();

        // Assert
        Assertions.assertNotNull(resultBody, "The result body should not be null");
        Assertions.assertFalse(resultBody.isEmpty(), "The result body should not be empty");
        ReviewDTO result = resultBody.getFirst();
        Assertions.assertEquals(review, result, "The returned review should match the expected review");
    }

    @Test
    void getReviewsByUserId_nonExistingUser_returnsEmptyList() {
        // Arrange
        Mockito.when(reviewFacade.getReviewsByUserId(1L)).thenReturn(java.util.Collections.emptyList());

        // Act
        List<ReviewDTO> result = reviewController.getReviewsByUserId(1L).getBody();

        // Assert

        Assertions.assertNotNull(result);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void getAllReviewsSortedByProperty_validPropertyAndOrder_returnsReviews() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        FilmProperty property = FilmProperty.PLOT;
        String order = "ASC";
        Mockito.when(reviewFacade.getAllReviewsSortedByProperty(property, false)).thenReturn(java.util.Collections.singletonList(review));

        // Act
        List<ReviewDTO> result = reviewController.getAllReviewsSortedByProperty(property, order).getBody();

        // Assert
        Assertions.assertNotNull(result, "Result should not be null");
        Assertions.assertFalse(result.isEmpty(), "Result should not be empty");
        Assertions.assertEquals(review, result.getFirst());
    }

    @Test
    void getAllReviewsSortedByProperty_invalidPropertyAndValidOrder_returnsEmptyList() {
        // Arrange
        String order = "ASC";
        Mockito.when(reviewFacade.getAllReviewsSortedByProperty(null, false)).thenReturn(java.util.Collections.emptyList());

        // Act
        List<ReviewDTO> result = reviewController.getAllReviewsSortedByProperty(null, order).getBody();

        // Assert
       Assertions.assertNotNull(result);
       Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void getAllReviewsSortedByProperty_validPropertyAndInvalidOrder_returnsEmptyList() {
        // Arrange
        FilmProperty property = FilmProperty.PLOT;
        Mockito.when(reviewFacade.getAllReviewsSortedByProperty(property, false)).thenReturn(java.util.Collections.emptyList());

        // Act
        List<ReviewDTO> result = reviewController.getAllReviewsSortedByProperty(property, "invalid").getBody();

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void getAllReviewsSortedByProperty_invalidPropertyAndInvalidOrder_returnsEmptyList() {
        // Arrange
        Mockito.when(reviewFacade.getAllReviewsSortedByProperty(null, false)).thenReturn(java.util.Collections.emptyList());

        // Act
        List<ReviewDTO> result = reviewController.getAllReviewsSortedByProperty(null, "invalid").getBody();

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertTrue(result.isEmpty());
    }


    @Test
    void getAllReviewsSortedByRating_validOrder_returnsReviews() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        String order = "ASC";
        Mockito.when(reviewFacade.getAllReviewsSortedByRating(false)).thenReturn(java.util.Collections.singletonList(review));

        // Act
        List<ReviewDTO> result = reviewController.getAllReviewssSortedByProperty(order).getBody(); // Adjusted method name to likely correct one


        // Assert
        Assertions.assertNotNull(result, "The result should not be null");
        Assertions.assertFalse(result.isEmpty(), "The result list should not be empty");
        Assertions.assertEquals(review, result.getFirst(), "The first element should match the expected review");
    }
}
