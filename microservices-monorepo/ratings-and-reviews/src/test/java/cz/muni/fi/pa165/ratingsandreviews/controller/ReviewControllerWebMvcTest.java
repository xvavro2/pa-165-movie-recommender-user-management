package cz.muni.fi.pa165.ratingsandreviews.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.facade.ReviewFacade;
import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;


import org.mockito.ArgumentMatchers;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@WebMvcTest(ReviewController.class)
class ReviewControllerWebMvcTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewFacade reviewFacade;

    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    void getReviewById_whenFound_returnsReview() throws Exception {
        // Arrange
        long reviewId = 1L;
        ReviewDTO expectedReview = new ReviewDTO();
        expectedReview.setId(reviewId);
        expectedReview.setMovieId(2L);
        expectedReview.setUserId(3L);
        expectedReview.setOverallRating(4.5);

        Mockito.when(reviewFacade.getReview(reviewId)).thenReturn(Optional.of(expectedReview));

        // Act & Assert
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/{reviewId}", reviewId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        Assertions.assertEquals("{\"movieId\":2,\"userId\":3,\"id\":1,\"ratings\":null,\"overallRating\":4.5}", responseJson);
    }

    @Test
    void getReviewById_whenNotFound_returnsNotFound() throws Exception {
        // Arrange
        long reviewId = 1L;
        Mockito.when(reviewFacade.getReview(reviewId)).thenReturn(Optional.empty());

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/{reviewId}", reviewId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }


    @Test
    void deleteReview_reviewExists_returnsDeleted() throws Exception {
        // Arrange
        long reviewId = 1L;
        ReviewDTO review = new ReviewDTO();
        review.setId(reviewId);
        Mockito.when(reviewFacade.deleteReview(reviewId)).thenReturn(true);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.delete("/api/reviews/{reviewId}", reviewId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        Assertions.assertEquals("1", responseJson);
    }

    @Test
    void deleteReview_reviewDoesNotExists_returnsNotFound() throws Exception {
        // Arrange
        long reviewId = 1L;
        Mockito.when(reviewFacade.deleteReview(reviewId)).thenReturn(false);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/reviews/{reviewId}", reviewId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void updateReview_reviewExists_returnsUpdatedReview() throws Exception {
        // Arrange
        long reviewId = 1L;
        ReviewDTO review = new ReviewDTO();
        review.setId(reviewId);
        ReviewDTO updatedReview = new ReviewDTO();
        updatedReview.setId(reviewId);
        Mockito.when(reviewFacade.updateReview(ArgumentMatchers.any(ReviewDTO.class))).thenReturn(updatedReview);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.put("/api/reviews/{reviewId}", reviewId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(review)).accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        Assertions.assertEquals("{\"movieId\":null,\"userId\":null,\"id\":1,\"ratings\":null,\"overallRating\":null}", responseJson);
    }

    @Test
    void getReview_reviewFound_returnsReview() throws Exception {
        // Arrange
        long reviewId = 1L;
        ReviewDTO expectedReview = new ReviewDTO();
        expectedReview.setId(reviewId);
        Mockito.when(reviewFacade.getReview(reviewId)).thenReturn(Optional.of(expectedReview));

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/{reviewId}", reviewId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        ReviewDTO response = mapper.readValue(responseJson, ReviewDTO.class);
        Assertions.assertEquals(expectedReview.getId(), response.getId());
        Assertions.assertEquals(expectedReview.getMovieId(), response.getMovieId());
        Assertions.assertEquals(expectedReview.getUserId(), response.getUserId());
    }

    @Test
    void getReview_reviewNotFound_returnsNotFound() throws Exception {
        // Arrange
        long reviewId = 1L;
        Mockito.when(reviewFacade.getReview(reviewId)).thenReturn(Optional.empty());

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/{reviewId}", reviewId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void getReviewsByUserId_userFound_returnsReviews() throws Exception {
        // Arrange
        long userId = 1L;
        ReviewDTO review = new ReviewDTO();
        review.setUserId(userId);
        Mockito.when(reviewFacade.getReviewsByUserId(userId)).thenReturn(List.of(new ReviewDTO[]{review}));

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/getByUserId").param("userId", String.valueOf(userId)))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        ReviewDTO[] response = mapper.readValue(responseJson, ReviewDTO[].class);
        Assertions.assertEquals(1, response.length);
        Assertions.assertEquals(userId, response[0].getUserId());
    }

    @Test
    void getAllReviewsSortedByProperty_reviewsFound_returnsReviews() throws Exception {
        // Arrange
        FilmProperty property = FilmProperty.PLOT;
        String order = "ASC";
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.getAllReviewsSortedByProperty(property, false)).thenReturn(List.of(new ReviewDTO[]{review}));

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/sortedByProperty/{property}/{order}", property, order))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        ReviewDTO[] response = mapper.readValue(responseJson, ReviewDTO[].class);
        Assertions.assertEquals(1, response.length);

    }

    @Test
    void getAllReviewssSortedByProperty_reviewsFound_returnsReviews() throws Exception {
        // Arrange
        String order = "ASC";
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.getAllReviewsSortedByRating(false)).thenReturn(List.of(new ReviewDTO[]{review}));

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/sortedByRating/{order}", order))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        ReviewDTO[] response = mapper.readValue(responseJson, ReviewDTO[].class);
        Assertions.assertEquals(1, response.length);
    }

}
