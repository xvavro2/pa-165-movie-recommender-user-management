package cz.muni.fi.pa165.ratingsandreviews.service;

import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.repository.ReviewRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class ReviewServiceImplTest {

    @Mock
    private ReviewRepository reviewRepository;

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Test
    void getAllReviews_returnsListOfReviews() {
        // Arrange
        List<Review> expectedReviews = Arrays.asList(new Review(), new Review());
        Mockito.when(reviewRepository.findAll()).thenReturn(expectedReviews);

        // Act
        List<Review> reviews = reviewService.getAllReviews();

        // Assert
        Assertions.assertEquals(expectedReviews, reviews);
        Mockito.verify(reviewRepository).findAll();
    }


    @Test
    void addReview_validReview_savesAndReturnsReview() {
        // Arrange
        Review newReview = new Review();
        Mockito.when(reviewRepository.save(newReview)).thenReturn(newReview);

        // Act
        Review savedReview = reviewService.addReview(newReview);

        // Assert
        Assertions.assertEquals(newReview, savedReview);
        Mockito.verify(reviewRepository).save(newReview);
    }

    @Test
    void deleteReview_existingId_deletesAndReturnsReview() {
        // Arrange
        Long reviewId = 1L;
        Review review = new Review();
        Mockito.when(reviewRepository.findById(reviewId)).thenReturn(Optional.of(review));

        // Act
        Boolean deletedReview = reviewService.deleteReview(reviewId);

        // Assert
        Assertions.assertTrue(deletedReview);
        Mockito.verify(reviewRepository).delete(review);
    }

    @Test
    void deleteReview_nonExistingId_throwsEntityNotFoundException() {
        // Arrange
        Long reviewId = 1L;
        Mockito.when(reviewRepository.findById(reviewId)).thenReturn(Optional.empty());

        // Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> reviewService.deleteReview(reviewId));
        Mockito.verify(reviewRepository, Mockito.never()).delete(ArgumentMatchers.any());
    }

    @Test
    void getReview_existingId_returnsReview() {
        // Arrange
        Long reviewId = 1L;
        Review review = new Review();
        Mockito.when(reviewRepository.findById(reviewId)).thenReturn(Optional.of(review));

        // Act
        Optional<Review> foundReview = reviewService.getReview(reviewId);

        // Assert
        Assertions.assertEquals(review, foundReview.get());
        Mockito.verify(reviewRepository).findById(reviewId);
    }

    @Test
    void getReview_nonExistingId_returnsEmpty() {
        // Arrange
        Long reviewId = 1L;
        Mockito.when(reviewRepository.findById(reviewId)).thenReturn(Optional.empty());

        Optional<Review> result = reviewService.getReview(1L);
        // Act & Assert
        Assertions.assertEquals(Optional.empty(), result);
    }

    @Test
    void updateReview_reviewAlreadyPresent_savesAndReturnsUpdatedReview() {
        // Arrange
        Review updatedReview = new Review();
        Mockito.when(reviewRepository.save(updatedReview)).thenReturn(updatedReview);

        // Act
        Review result = reviewService.updateReview(updatedReview);

        // Assert
        Assertions.assertEquals(updatedReview, result);
        Mockito.verify(reviewRepository).save(updatedReview);
    }

    @Test
    void updateReview_notExistingReview_storesNewReview() {
        // Arrange
        Review updatedReview = new Review();
        updatedReview.setId(1L);
        Mockito.when(reviewRepository.save(updatedReview)).thenReturn(updatedReview);
        // Act
        Review result = reviewService.updateReview(updatedReview);

        // Assert
        Assertions.assertEquals(updatedReview, result);
        Mockito.verify(reviewRepository).save(updatedReview);
    }
}
