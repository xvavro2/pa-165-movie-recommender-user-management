package cz.muni.fi.pa165.ratingsandreviews.facade;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.mapper.ReviewMapper;
import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.service.ReviewService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.mockito.Mockito;

@ExtendWith(MockitoExtension.class)
class ReviewFacadeImplTest {

    @Mock
    ReviewService reviewService;

    @Mock
    ReviewMapper reviewMapper;

    @InjectMocks
    ReviewFacadeImpl reviewFacade;

    @Test
    void addReview_validReview_returnsNewReview() {
        // Arrange
        ReviewDTO newReviewDTO = new ReviewDTO();
        Review newReview = new Review();
        Mockito.when(reviewMapper.dtoToEntity(newReviewDTO)).thenReturn(newReview);
        Mockito.when(reviewService.addReview(newReview)).thenReturn(newReview);

        // Act
        ReviewDTO result = reviewFacade.addReview(newReviewDTO);

        // Assert
        Assertions.assertEquals(newReviewDTO, result);
        Mockito.verify(reviewService).addReview(newReview);
        Mockito.verify(reviewMapper).dtoToEntity(newReviewDTO);
    }

    @Test
    void addReview_invalidReview_throwsException() {
        // Arrange
        ReviewDTO newReviewDTO = new ReviewDTO();
        Review newReview = new Review();
        Mockito.when(reviewMapper.dtoToEntity(newReviewDTO)).thenReturn(newReview);
        Mockito.when(reviewService.addReview(newReview)).thenThrow(new IllegalArgumentException());

        // Act and Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> reviewFacade.addReview(newReviewDTO));
    }

    @Test
    void deleteReview_validId_returnsDeletedReview() {
        // Arrange
        long reviewId = 1L;
        Mockito.when(reviewService.deleteReview(reviewId)).thenReturn(true);

        // Act
        Boolean result = reviewFacade.deleteReview(reviewId);

        // Assert
        Assertions.assertEquals(true, result);
        Mockito.verify(reviewService).deleteReview(reviewId);
    }

    @Test
    void updateReview_validReview_returnsUpdatedReview() {
        // Arrange
        ReviewDTO updatedReviewDTO = new ReviewDTO();
        Review updatedReview = new Review();
        Mockito.when(reviewMapper.dtoToEntity(updatedReviewDTO)).thenReturn(updatedReview);
        Mockito.when(reviewService.addReview(updatedReview)).thenReturn(updatedReview);

        // Act
        ReviewDTO result = reviewFacade.updateReview(updatedReviewDTO);

        // Assert
        Assertions.assertEquals(updatedReviewDTO, result);
        Mockito.verify(reviewService).addReview(updatedReview);
        Mockito.verify(reviewMapper).dtoToEntity(updatedReviewDTO);
    }

    @Test
    void updateReview_invalidReview_throwsException() {
        // Arrange
        ReviewDTO updatedReviewDTO = new ReviewDTO();
        Review updatedReview = new Review();
        Mockito.when(reviewMapper.dtoToEntity(updatedReviewDTO)).thenReturn(updatedReview);
        Mockito.when(reviewService.addReview(updatedReview)).thenThrow(new IllegalArgumentException());

        // Act and Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> reviewFacade.updateReview(updatedReviewDTO));
    }


    @Test
    void getReview_validId_returnsReview() {
        // Arrange
        long reviewId = 1L;
        Review review = new Review();
        ReviewDTO reviewDTO = new ReviewDTO();
        Mockito.when(reviewService.getReview(reviewId)).thenReturn(Optional.of(review));
        Mockito.when(reviewMapper.entityToDto(review)).thenReturn(reviewDTO);

        // Act
        Optional<ReviewDTO> result = reviewFacade.getReview(reviewId);

        // Assert
        Assertions.assertEquals(reviewDTO, result.get());
        Mockito.verify(reviewService).getReview(reviewId);
        Mockito.verify(reviewMapper).entityToDto(review);
    }

    @Test
    void getReviewsByUserId_validId_returnsReviewsList() {
        // Arrange
        long userId = 1L;
        Review review1 = new Review();
        Review review2 = new Review();
        review1.setId(1L);
        review1.setUserId(userId);
        review2.setId(2L);
        review2.setUserId(userId);
        List<Review> reviews = Arrays.asList(review1, review2);
        List<ReviewDTO> reviewDTOs = reviews.stream()
                .map(review -> new ReviewDTO())
                .toList();
        Mockito.when(reviewService.getAllReviews()).thenReturn(reviews);
        Mockito.when(reviewMapper.entityToDto(ArgumentMatchers.any(Review.class))).thenAnswer(i -> new ReviewDTO());

        // Act
        List<ReviewDTO> result = reviewFacade.getReviewsByUserId(userId);

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertEquals(reviewDTOs.size(), result.size());
        Mockito.verify(reviewService).getAllReviews();
    }

    @Test
    void getReviewsByUserId_invalidId_returnsEmptyList() {
        // Arrange
        long userId = 1L;
        List<Review> reviews = Arrays.asList(new Review(), new Review());
        Mockito.when(reviewService.getAllReviews()).thenReturn(reviews);

        // Act
        List<ReviewDTO> result = reviewFacade.getReviewsByUserId(userId);

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertEquals(0, result.size());
        Mockito.verify(reviewService).getAllReviews();
    }


    @Test
    void getAllReviewsSortedByRating_validState_returnsSortedReviews() {
        // Arrange
        List<Review> reviews = Arrays.asList(new Review(), new Review());
        List<ReviewDTO> reviewDTOs = reviews.stream()
                .map(review -> new ReviewDTO())
                .toList();
        Mockito.when(reviewService.getAllReviews()).thenReturn(reviews);
        Mockito.when(reviewMapper.entityToDto(ArgumentMatchers.any(Review.class))).thenAnswer(i -> new ReviewDTO());

        // Act
        List<ReviewDTO> result = reviewFacade.getAllReviewsSortedByRating(true);

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertEquals(reviewDTOs.size(), result.size());
        Mockito.verify(reviewService).getAllReviews();
    }

    @Test
    void getAllReviewsSortedByRating_NoReviews_returnsEmptyList() {
        // Arrange
        Mockito.when(reviewService.getAllReviews()).thenReturn(Collections.emptyList());

        // Act
        List<ReviewDTO> result = reviewFacade.getAllReviewsSortedByRating(true);

        // Assert
        Assertions.assertTrue(result.isEmpty());
        Mockito.verify(reviewService).getAllReviews();
    }


    @Test
    void getAllReviewsSortedByProperty_validArguments_returnsSortedReviews() {
        // Arrange
        FilmProperty property = FilmProperty.PLOT;
        List<Review> reviews = Arrays.asList(new Review(), new Review());
        List<ReviewDTO> reviewDTOs = reviews.stream().map(review -> new ReviewDTO()).toList();
        Mockito.when(reviewService.getAllReviews()).thenReturn(reviews);
        Mockito.when(reviewMapper.entityToDto(ArgumentMatchers.any(Review.class))).thenAnswer(i -> new ReviewDTO());

        // Act
        List<ReviewDTO> result = reviewFacade.getAllReviewsSortedByProperty(property, true);

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertEquals(reviewDTOs.size(), result.size());
        Mockito.verify(reviewService).getAllReviews();
    }
}
