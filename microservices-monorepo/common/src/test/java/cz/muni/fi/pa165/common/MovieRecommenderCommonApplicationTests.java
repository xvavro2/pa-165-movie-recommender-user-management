package cz.muni.fi.pa165.common;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovieRecommenderCommonApplicationTests {

	@Test
	void contextLoads() {
		// Intentionally empty
	}

}
