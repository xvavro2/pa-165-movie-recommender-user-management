package cz.muni.fi.pa165.recommendations.data.repository;

import cz.muni.fi.pa165.recommendations.data.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IMoviesRepositoryMock {

	/**
	 * Returns all movies
	 *
	 * @return list of movies
	 */
	List<Movie> getAll();

	/**
	 * Returns movie by its id
	 *
	 * @param id movie id
	 * @return movie
	 */
	Optional<Movie> findById(Long id);

	/**
	 * Adds new movie to repository
	 *
	 * @param movie movie
	 */
	void add(Movie movie);

	/**
	 * Deletes movie from repository
	 *
	 * @param movie movie
	 */
	void delete(Movie movie);

	/**
	 * Updates movie in repository
	 *
	 * @param movie movie
	 */
	void update(Movie movie);
}
