package cz.muni.fi.pa165.recommendations.facade;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import cz.muni.fi.pa165.recommendations.mappers.MovieMapper;
import cz.muni.fi.pa165.recommendations.service.RecommendationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RecommendationsFacade implements IRecommendationsFacade {

	private final RecommendationsService recommendationsService;
	private final MovieMapper movieMapper;

	/**
	 * RecommendationsFacade constructor
	 *
	 * @param recommendationsService recommendations service class
	 * @param movieMapper movie mapper class
	 */
	@Autowired
	public RecommendationsFacade(
			final RecommendationsService recommendationsService,
			final MovieMapper movieMapper
	) {
		this.recommendationsService = recommendationsService;
		this.movieMapper = movieMapper;
	}

	/**
	 * Get 10 recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @return List of recommended movies
	 */
	@Override
	@Transactional(readOnly = true)
	public List<MovieDTO> getRecommendedMovies(final Long movieId) {
		List<Movie> recommendedMovies = recommendationsService.getRecommendedMovies(movieId);
		return movieMapper.mapToMovieDTOList(recommendedMovies);
	}

	/**
	 * Get parametrized number of recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @param count number of recommended movies to return
	 * @return List of recommended movies
	 */
	@Override
	@Transactional(readOnly = true)
	public List<MovieDTO> getRecommendedMovies(final Long movieId, final Integer count) {
		List<Movie> recommendedMovies = recommendationsService.getRecommendedMovies(movieId, count);
		return movieMapper.mapToMovieDTOList(recommendedMovies);
	}
}
