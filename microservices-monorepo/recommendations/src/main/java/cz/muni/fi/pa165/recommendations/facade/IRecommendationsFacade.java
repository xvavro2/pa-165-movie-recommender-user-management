package cz.muni.fi.pa165.recommendations.facade;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IRecommendationsFacade {

	/**
	 * Get 10 recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @return List of recommended movies
	 */
	List<MovieDTO> getRecommendedMovies(Long movieId);

	/**
	 * Get parametrized number of recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @param count number of recommended movies to return
	 * @return List of recommended movies
	 */
	List<MovieDTO> getRecommendedMovies(Long movieId, Integer count);
}
