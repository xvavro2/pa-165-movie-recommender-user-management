package cz.muni.fi.pa165.recommendations.rest;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.facade.RecommendationsFacade;
import cz.muni.fi.pa165.recommendations.util.ObjectConveter;
import cz.muni.fi.pa165.recommendations.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {RecommendationsRestController.class})
public class RecommendationsRestControllerWebMvcTest {
    private static final long movieId = 1L;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecommendationsFacade recommendationsFacade;

    @Test
    void getRecommendedMovies_ValidId_SuccessAndRecommendations() throws Exception {
        // Arrange
        List<MovieDTO> movies = TestDataFactory.createMovieDTOList();
        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId))
            .thenReturn(movies);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/recommendations/{id}", movieId)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        List<MovieDTO> resultMovies = ObjectConveter.convertJsonToListOfMovieDTO(responseJson);
        assertEqualityOfDtos(movies, resultMovies);
        Mockito.verify(recommendationsFacade, Mockito.times(1)).getRecommendedMovies(movieId);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCount_SuccessAndSpecificNumberOfRecommendations() throws Exception {
        // Arrange
        int count = 5;
        List<MovieDTO> movies = TestDataFactory.createMovieDTOList(count);
        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId, count))
            .thenReturn(movies);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/recommendations/{id}?count={count}",
                    movieId, count)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        List<MovieDTO> resultMovies = ObjectConveter.convertJsonToListOfMovieDTO(responseJson);
        assertEqualityOfDtos(movies, resultMovies);
        Mockito.verify(recommendationsFacade, Mockito.times(1)).getRecommendedMovies(movieId, count);
    }

    @Test
    void seekMyBeans() {
        System.out.println("** My beans are **");
        Arrays.stream(applicationContext.getBeanDefinitionNames())
            .filter(beanName -> applicationContext.getBean(beanName)
                .getClass()
                .getPackageName()
                .startsWith("cz.muni.fi.pa165")
            )
            .forEach(System.out::println);
    }


    private void assertEqualityOfDtos(List<MovieDTO> result, List<MovieDTO> movieDTOs) {
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(movieDTOs.size());
        assertThat(result).containsAll(movieDTOs);
    }
}
