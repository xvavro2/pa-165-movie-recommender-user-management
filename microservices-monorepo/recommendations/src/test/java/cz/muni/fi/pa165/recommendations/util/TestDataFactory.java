package cz.muni.fi.pa165.recommendations.util;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.data.enums.MovieGenre;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cz.muni.fi.pa165.recommendations.service.RecommendationsService.DEFAULT_RECOMMENDATIONS;

@Component
public final class TestDataFactory {

    private TestDataFactory() {
    }

    /**
     * Creates a single Movie instance.
     *
     * @param id The ID of the movie.
     * @return A Movie object.
     */
    public static Movie createMovie(Long id) {
        return new Movie(
            id,
            "Sample Movie Title " + id,
            2020, // example year
            "Sample Director " + id,
            new ArrayList<>(Arrays.asList(MovieGenre.ACTION, MovieGenre.COMEDY)), // example genres
            new ArrayList<>(Arrays.asList("Actor A", "Actor B")), // example actors
            8.5 // example rating
        );
    }

    /**
     * Creates a list of Movie instances for testing, with a specific count.
     *
     * @param count The number of movies to create.
     * @return A list of Movie objects.
     */
    public static List<Movie> createMovieList(int count) {
        List<Movie> movies = new ArrayList<>();
        for (long i = 1; i <= count; i++) {
            movies.add(createMovie(i));
        }
        return movies;
    }

    public static List<Movie> createMovieList() {
        return createMovieList(DEFAULT_RECOMMENDATIONS);
    }

    /**
     * Creates a list of MovieDTO instances for testing, with a specific count.
     *
     * @param count The number of MovieDTOs to create.
     * @return A list of MovieDTO objects.
     */
    public static List<MovieDTO> createMovieDTOList(int count) {
        List<MovieDTO> movieDTOs = new ArrayList<>();
        for (long i = 1; i <= count; i++) {
            Movie movie = createMovie(i);
            movieDTOs.add(new MovieDTO(
                movie.getId(),
                movie.getTitle(),
                movie.getDirector(),
                movie.getGenres(),
                movie.getActors(),
                movie.getRating()
            ));
        }
        return movieDTOs;
    }

    public static List<MovieDTO> createMovieDTOList() {
        return createMovieDTOList(DEFAULT_RECOMMENDATIONS);
    }
}
