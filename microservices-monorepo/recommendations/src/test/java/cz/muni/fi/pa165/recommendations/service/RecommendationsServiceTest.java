package cz.muni.fi.pa165.recommendations.service;

import cz.muni.fi.pa165.recommendations.data.model.Movie;
import cz.muni.fi.pa165.recommendations.data.repository.MoviesRepositoryMock;
import cz.muni.fi.pa165.recommendations.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static cz.muni.fi.pa165.recommendations.service.RecommendationsService.DEFAULT_RECOMMENDATIONS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RecommendationsServiceTest {
    private static final long movieId = 1L;

    @InjectMocks
    private RecommendationsService recommendationsService;

    @Mock
    private MoviesRepositoryMock moviesRepositoryMock;

    @Test
    void getRecommendedMovies_ValidId_ReturnsMovieList() {
        // Arrange
        List<Movie> movies = TestDataFactory.createMovieList();

        Mockito.when(moviesRepositoryMock.findById(movieId)).thenReturn(Optional.of(TestDataFactory.createMovie(movieId)));
        Mockito.when(moviesRepositoryMock.getAll()).thenReturn(movies);

        // Act
        List<Movie> recommendedMovies = recommendationsService.getRecommendedMovies(movieId);

        // Assert
        assertResultStatus(recommendedMovies, movies);
    }

    @Test
    void getRecommendedMovies_InvalidId_ThrowsException() {
        // Arrange
        Mockito.when(moviesRepositoryMock.findById(movieId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(RuntimeException.class, () -> recommendationsService.getRecommendedMovies(movieId));
        verify(moviesRepositoryMock, times(1)).findById(1L);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCount_ReturnsMovieList() {
        // Arrange
        int count = 5;

        List<Movie> movies = TestDataFactory.createMovieList(count);
        Mockito.when(moviesRepositoryMock.findById(movieId)).thenReturn(Optional.of(TestDataFactory.createMovie(movieId)));
        Mockito.when(moviesRepositoryMock.getAll()).thenReturn(movies);

        // Act
        List<Movie> recommendedMovies = recommendationsService.getRecommendedMovies(movieId, count);

        // Assert
        assertResultStatus(recommendedMovies, movies);
    }

    @Test
    void getRecommendedMovies_InvalidIdSpecificCount_ThrowsException() {
        // Arrange
        int count = 5;
        Mockito.when(moviesRepositoryMock.findById(movieId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(RuntimeException.class, () -> recommendationsService.getRecommendedMovies(movieId, count));
        verify(moviesRepositoryMock, times(1)).findById(1L);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCountLessMovies_ReturnsAllMoviesList() {
        // Arrange
        int realCount = 2;
        int count = 5;

        List<Movie> movies = TestDataFactory.createMovieList(realCount);
        Mockito.when(moviesRepositoryMock.findById(movieId)).thenReturn(Optional.of(TestDataFactory.createMovie(movieId)));
        Mockito.when(moviesRepositoryMock.getAll()).thenReturn(movies);

        // Act
        List<Movie> recommendedMovies = recommendationsService.getRecommendedMovies(movieId, count);

        // Assert
        assertResultStatus(recommendedMovies, movies);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCountMoreMovies_ReturnsCountMovies() {
        // Arrange
        int realCount = 15;
        int count = 5;

        List<Movie> movies = TestDataFactory.createMovieList(realCount);
        Mockito.when(moviesRepositoryMock.findById(movieId)).thenReturn(Optional.of(TestDataFactory.createMovie(movieId)));
        Mockito.when(moviesRepositoryMock.getAll()).thenReturn(movies);

        // Act
        List<Movie> recommendedMovies = recommendationsService.getRecommendedMovies(movieId, count);

        // Assert
        assertThat(recommendedMovies).isNotEmpty();
        assertThat(recommendedMovies.size()).isEqualTo(count);
        verifyMethodCalls();
    }

    @Test
    void getRecommendedMovies_ValidIdMoreMovies_Returns10Movies() {
        // Arrange
        int realCount = 15;

        List<Movie> movies = TestDataFactory.createMovieList(realCount);
        Mockito.when(moviesRepositoryMock.findById(movieId)).thenReturn(Optional.of(TestDataFactory.createMovie(movieId)));
        Mockito.when(moviesRepositoryMock.getAll()).thenReturn(movies);

        // Act
        List<Movie> recommendedMovies = recommendationsService.getRecommendedMovies(movieId);

        // Assert
        assertThat(recommendedMovies).isNotEmpty();
        assertThat(recommendedMovies.size()).isEqualTo(DEFAULT_RECOMMENDATIONS);
        verifyMethodCalls();
    }


    private void assertResultStatus(List<Movie> recommendedMovies, List<Movie> expectedMovies) {
        assertThat(recommendedMovies).isNotEmpty();
        assertThat(recommendedMovies.size()).isEqualTo(expectedMovies.size());
        assertThat(recommendedMovies).containsAll(expectedMovies);
        verifyMethodCalls();
    }

    private void verifyMethodCalls() {
        verify(moviesRepositoryMock, times(1)).findById(1L);
        verify(moviesRepositoryMock, times(1)).getAll();
    }
}
