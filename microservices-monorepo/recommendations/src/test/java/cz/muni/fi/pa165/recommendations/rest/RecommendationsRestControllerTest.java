package cz.muni.fi.pa165.recommendations.rest;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.facade.RecommendationsFacade;
import cz.muni.fi.pa165.recommendations.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RecommendationsRestControllerTest {
    private static final long movieId = 1L;

    @Mock
    private RecommendationsFacade recommendationsFacade;

    @InjectMocks
    private RecommendationsRestController restController;

    @Test
    void getRecommendedMovies_ValidId_200StatusCodeAnd10DTOClasses() {
        // Arrange
        List<MovieDTO> movieDTOs = TestDataFactory.createMovieDTOList();

        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId)).thenReturn(movieDTOs);

        // Act
        ResponseEntity<List<MovieDTO>> result = restController.getRecommendedMovies(movieId);

        // Assert
        assertSuccessfulCall(result, movieDTOs);
        verify(recommendationsFacade, times(1)).getRecommendedMovies(movieId);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCount_200StatusCodeAndCountDTOClasses() {
        // Arrange
        int count = 5;
        List<MovieDTO> movieDTOs = TestDataFactory.createMovieDTOList(count);

        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId, count)).thenReturn(movieDTOs);

        // Act
        ResponseEntity<List<MovieDTO>> result = restController.getRecommendedMovies(movieId, count);

        // Assert
        assertSuccessfulCall(result, movieDTOs);
        verify(recommendationsFacade, times(1)).getRecommendedMovies(movieId, count);
    }

//    No unsuccessful calls handling yet, so cannot test it
//    @Test
//    void getRecommendedMovies_InvalidIdSpecificCount_NotFound() {
//        // Arrange
//        int count = 5;
//
//        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId, count))
//            .thenThrow(RuntimeException.class);
//
//        // Act
//        ResponseEntity<List<MovieDTO>> result = restController.getRecommendedMovies(movieId, count);
//
//        // Assert
//        assertUnsuccessfulCall(result);
//        verify(recommendationsFacade, times(1)).getRecommendedMovies(movieId, count);
//    }
//
//    @Test
//    void getRecommendedMovies_InvalidId_NotFound() {
//        // Arrange
//        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId))
//            .thenThrow(RuntimeException.class);
//
//        // Act
//        ResponseEntity<List<MovieDTO>> result = restController.getRecommendedMovies(movieId);
//
//        // Assert
//        assertUnsuccessfulCall(result);
//        verify(recommendationsFacade, times(1)).getRecommendedMovies(movieId);
//    }

    private void assertSuccessfulCall(ResponseEntity<List<MovieDTO>> result, List<MovieDTO> movieDTOs) {
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        List<MovieDTO> resultMovies = result.getBody();
        assertResultStatus(movieDTOs, resultMovies);
    }


//    private void assertUnsuccessfulCall(ResponseEntity<List<MovieDTO>> result) {
//        assertThat(result).isNotNull();
//        assertThat(result.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(404));
//        assertThat(result.getBody()).isNull();
//    }

    private void assertResultStatus(List<MovieDTO> recommendedMovies, List<MovieDTO> expectedMovies) {
        assertThat(recommendedMovies).isNotEmpty();
        assertThat(recommendedMovies.size()).isEqualTo(expectedMovies.size());
        assertThat(recommendedMovies).containsAll(expectedMovies);
    }
}
