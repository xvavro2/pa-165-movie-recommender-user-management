package cz.muni.fi.pa165.recommendations.facade;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import cz.muni.fi.pa165.recommendations.mappers.MovieMapper;
import cz.muni.fi.pa165.recommendations.service.RecommendationsService;
import cz.muni.fi.pa165.recommendations.util.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RecommendationsFacadeTest {
    private static final long movieId = 1L;

    @Mock
    private RecommendationsService recommendationsService;

    @Mock
    private MovieMapper movieMapper;

    @InjectMocks
    private RecommendationsFacade recommendationsFacade;

    @Test
    void getRecommendedMovies_ValidId_MovieDTOList() {
        // Arrange
        List<Movie> movies = TestDataFactory.createMovieList();
        List<MovieDTO> movieDTOs = TestDataFactory.createMovieDTOList();

        Mockito.when(recommendationsService.getRecommendedMovies(movieId)).thenReturn(movies);
        Mockito.when(movieMapper.mapToMovieDTOList(movies)).thenReturn(movieDTOs);

        // Act
        List<MovieDTO> result = recommendationsFacade.getRecommendedMovies(movieId);

        // Assert
        assertSuccessfulResult(result, movieDTOs);
        verify(recommendationsService, times(1)).getRecommendedMovies(movieId);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCount_CountMovieDTOList() {
        // Arrange
        int count = 5;
        List<Movie> movies = TestDataFactory.createMovieList(count);
        List<MovieDTO> movieDTOs = TestDataFactory.createMovieDTOList(count);

        Mockito.when(recommendationsService.getRecommendedMovies(movieId, count)).thenReturn(movies);
        Mockito.when(movieMapper.mapToMovieDTOList(movies)).thenReturn(movieDTOs);

        // Act
        List<MovieDTO> result = recommendationsFacade.getRecommendedMovies(movieId, count);

        // Assert
        assertSuccessfulResult(result, movieDTOs);
        verify(recommendationsService, times(1)).getRecommendedMovies(movieId, count);
    }

    @Test
    void getRecommendedMovies_InvalidId_ThrowsException() {
        // Arrange
        // RuntimeException is thrown by the original class
        Mockito.when(recommendationsService.getRecommendedMovies(movieId)).thenThrow(RuntimeException.class);

        // Act & Assert
        assertThrows(RuntimeException.class, () -> recommendationsFacade.getRecommendedMovies(movieId),
            "Exception was not thrown");

        verify(recommendationsService, times(1)).getRecommendedMovies(movieId);
        verify(movieMapper, times(0)).mapToMovieDTOList(any(List.class));
    }

    @Test
    void getRecommendedMovies_InvalidIdSpecificCount_ThrowsException() {
        // Arrange
        int count = 5;
        // RuntimeException is thrown by the original class
        Mockito.when(recommendationsService.getRecommendedMovies(movieId, count)).thenThrow(RuntimeException.class);

        // Act & Assert
        assertThrows(RuntimeException.class, () -> recommendationsFacade.getRecommendedMovies(movieId, count),
            "Exception was not thrown");

        verify(recommendationsService, times(1)).getRecommendedMovies(movieId, count);
        verify(movieMapper, times(0)).mapToMovieDTOList(any(List.class));
    }

    private void assertSuccessfulResult(List<MovieDTO> result, List<MovieDTO> movieDTOs) {
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(movieDTOs.size());
        assertThat(result).containsAll(movieDTOs);
        verify(movieMapper, times(1)).mapToMovieDTOList(any(List.class));
    }
}
