package cz.muni.fi.pa165.usermanagement.rest;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.facade.UserFacadeImpl;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;


@ExtendWith(MockitoExtension.class)
class UserRestControllerTest {

	/**
	 * Successful response code.
	 */
	public static final int RESPONSE_OK = 200;

	/**
	 * Not found response code.
	 */
	public static final int RESPONSE_NOT_FOUND = 204;

	@Mock
	private UserFacadeImpl userFacade;

	@InjectMocks
	private UserRestController userRestController;

	/**
	 * Test method for {@link UserRestController#getAllUsers()}.
	 */
	@Test
	void getAllUsers() {
		List<UserDTO> userDTOs = TestDataFactory.createUserDTOMultiple(10L);

		Mockito.when(userFacade.getAllUsers()).thenReturn(userDTOs);

		ResponseEntity<List<UserDTO>> response = userRestController.getAllUsers();

		Assertions.assertNotNull(response);
		Assertions.assertEquals(response.getStatusCode(), HttpStatusCode.valueOf(RESPONSE_OK));
		Assertions.assertEquals(response.getBody(), userDTOs);

		Mockito.verify(userFacade, Mockito.times(1)).getAllUsers();
	}

	/**
	 * Test method for {@link UserRestController#createUser(UserRegistrationDTO)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void createUser(final UserType userType) {
		UserRegistrationDTO userRegistrationDTO = TestDataFactory.createUserRegistrationDTO("newUser", userType);
		UserDTO userDTO = TestDataFactory.createUserDTO(1L, userType);

		Mockito.when(userFacade.createUser(userRegistrationDTO)).thenReturn(userDTO);
		ResponseEntity<UserDTO> response = userRestController.createUser(userRegistrationDTO);

		Assertions.assertNotNull(response);
		Assertions.assertEquals(response.getStatusCode(), HttpStatusCode.valueOf(RESPONSE_OK));
		Assertions.assertNotNull(response.getBody());
		Assertions.assertEquals(response.getBody().userName(), userDTO.userName());
		Assertions.assertEquals(response.getBody().email(), userDTO.email());
		Assertions.assertEquals(response.getBody().userType(), userType);

		Mockito.verify(userFacade, Mockito.times(1)).createUser(userRegistrationDTO);
	}

	/**
	 * Test method for {@link UserRestController#createUser(UserRegistrationDTO)}.
	 */
	@Test
	void getUser_success() {
		UserDTO userDTO = TestDataFactory.createUserDTO(1L, UserType.USER);

		Mockito.when(userFacade.getUser(1L)).thenReturn(userDTO);

		ResponseEntity<UserDTO> response = userRestController.getUser(1L);

		Assertions.assertNotNull(response);
		Assertions.assertEquals(response.getStatusCode(), HttpStatusCode.valueOf(RESPONSE_OK));
		Assertions.assertEquals(response.getBody(), userDTO);

		Mockito.verify(userFacade, Mockito.times(1)).getUser(1L);
	}

	/**
	 * Test method for {@link UserRestController#getUser(long)}.
	 */
	@Test
	void getUser_notFound() {
		Mockito.when(userFacade.getUser(1L)).thenReturn(null);
		ResponseEntity<UserDTO> response = userRestController.getUser(1L);

		Assertions.assertNotNull(response);
		Assertions.assertEquals(response.getStatusCode(), HttpStatusCode.valueOf(RESPONSE_OK));
		Assertions.assertNull(response.getBody());

		Mockito.verify(userFacade, Mockito.times(1)).getUser(1L);
	}

	/**
	 * Test method for {@link UserRestController#updateUser(long, UserUpdateDTO)}.
	 */
	@Test
	void updateUser_success() {
		UserRegistrationDTO userRegistrationDTO = TestDataFactory.createUserRegistrationDTO("newUser", UserType.USER);
		UserDTO userDTO = TestDataFactory.createUserDTO(1L, UserType.USER);
		UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("updatedUser", UserType.ADMIN);

		Mockito.when(userFacade.createUser(userRegistrationDTO)).thenReturn(userDTO);
		ResponseEntity<UserDTO> registrationResponse = userRestController.createUser(userRegistrationDTO);

		Assertions.assertNotNull(registrationResponse);
		Assertions.assertNotNull(registrationResponse.getBody());
		ResponseEntity<UserDTO> response = userRestController.updateUser(registrationResponse.getBody().id(), userUpdateDTO);

		Assertions.assertNotNull(response);
		Assertions.assertEquals(response.getStatusCode(), HttpStatusCode.valueOf(RESPONSE_NOT_FOUND));
		Assertions.assertNull(response.getBody());

		Mockito.verify(userFacade, Mockito.times(1)).updateUser(registrationResponse.getBody().id(), userUpdateDTO);
	}

	/**
	 * Test method for {@link UserRestController#updateUser(long, UserUpdateDTO)}.
	 */
	@Test
	void updateUser_notFound() {
		UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("updatedUser", UserType.ADMIN);

		ResponseEntity<UserDTO> response = userRestController.updateUser(1L, userUpdateDTO);

		Assertions.assertNotNull(response);
		Assertions.assertEquals(response.getStatusCode(), HttpStatusCode.valueOf(RESPONSE_NOT_FOUND));
		Assertions.assertNull(response.getBody());

		Mockito.verify(userFacade, Mockito.times(1)).updateUser(1L, userUpdateDTO);
	}

	/**
	 * Test method for {@link UserRestController#deleteUser(long)}.
	 */
	@Test
	void deleteUser() {
		// deleteUser doesn't return anything yet, can't test success/failure
		ResponseEntity<Void> response = userRestController.deleteUser(1L);

		Assertions.assertNotNull(response);
		Assertions.assertEquals(response.getStatusCode(), HttpStatusCode.valueOf(RESPONSE_NOT_FOUND));
		Assertions.assertNull(response.getBody());

		Mockito.verify(userFacade, Mockito.times(1)).deleteUser(1L);
	}
}