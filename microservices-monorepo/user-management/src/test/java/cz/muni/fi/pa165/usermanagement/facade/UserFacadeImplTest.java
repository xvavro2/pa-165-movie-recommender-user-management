package cz.muni.fi.pa165.usermanagement.facade;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.service.UserServiceImpl;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import cz.muni.fi.pa165.usermanagement.util.mappers.UserMapper;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;


@ExtendWith(MockitoExtension.class)
class UserFacadeImplTest {

	/**
	 * The ID of the first created user.
	 */
	public static final long FIRST_USER_ID = 1L;

	@Mock
	private UserServiceImpl userService;

	@Mock
	private UserMapper userMapper;

	@InjectMocks
	private UserFacadeImpl userFacade;

	/**
	 * Test method for {@link UserFacadeImpl#getAllUsers()}.
	 */
	@Test
	void getAllUsers() {
		List<User> users = TestDataFactory.createUserMultiple(5L);
		List<UserDTO> userDTOs = TestDataFactory.createUserDTOMultiple(5L);

		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Mockito.when(userMapper.usersToUserDTOs(users)).thenReturn(userDTOs);

		List<UserDTO> result = userFacade.getAllUsers();

		Assertions.assertNotNull(result);
		Assertions.assertEquals(userDTOs.size(), result.size());

		for (int i = 0; i < userDTOs.size(); i++) {
			Assertions.assertEquals(userDTOs.get(i).id(), result.get(i).id());
			Assertions.assertEquals(userDTOs.get(i).userName(), result.get(i).userName());
			Assertions.assertEquals(userDTOs.get(i).email(), result.get(i).email());
			Assertions.assertEquals(userDTOs.get(i).userType(), result.get(i).userType());
		}
		Mockito.verify(userService, Mockito.times(1)).getAllUsers();
	}

	/**
	 * Test method for {@link UserFacadeImpl#createUser(UserRegistrationDTO)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void createUser(final UserType userType) {
		User user = TestDataFactory.createUser(FIRST_USER_ID, userType);
		UserDTO userDTO = TestDataFactory.createUserDTO(FIRST_USER_ID, userType);
		UserRegistrationDTO userRegistrationDTO = TestDataFactory.createUserRegistrationDTO(
				"newUser", userType
		);

		Mockito.when(userMapper.userRegistrationDTOToUser(userRegistrationDTO)).thenReturn(user);
		Mockito.when(userService.createUser(user)).thenReturn(user);
		Mockito.when(userMapper.userToUserDTO(user)).thenReturn(userDTO);

		UserDTO result = userFacade.createUser(userRegistrationDTO);

		Assertions.assertNotNull(result);
		Assertions.assertEquals(userDTO.id(), result.id());
		Assertions.assertEquals(userDTO.userName(), result.userName());
		Assertions.assertEquals(userDTO.email(), result.email());
		Assertions.assertEquals(userDTO.userType(), userType);

		Mockito.verify(userService, Mockito.times(1)).createUser(user);
	}

	/**
	 * Test method for {@link UserFacadeImpl#updateUser(long, UserUpdateDTO)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void updateUser_success(final UserType userType) {
		User user = TestDataFactory.createUser(FIRST_USER_ID, userType);
		UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("newUser", userType);

		Mockito.when(userService.getUser(FIRST_USER_ID)).thenReturn(user);

		userFacade.updateUser(FIRST_USER_ID, userUpdateDTO);

		Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
		Mockito.verify(userService, Mockito.times(1)).updateUser(user);
	}

	/**
	 * Test method for {@link UserFacadeImpl#updateUser(long, UserUpdateDTO)}.
	 */
	@Test
	void updateUser_notFound() {
		UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("newUser", UserType.USER);
		Mockito.when(userService.getUser(FIRST_USER_ID)).thenThrow(EntityNotFoundException.class);

		Assertions.assertThrows(
				EntityNotFoundException.class, () -> userFacade.updateUser(FIRST_USER_ID, userUpdateDTO)
		);

		Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
	}

	/**
	 * Test method for {@link UserFacadeImpl#getUser(long)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void getUser_success(final UserType userType) {
		User user = TestDataFactory.createUser(FIRST_USER_ID, userType);
		UserDTO userDTO = TestDataFactory.createUserDTO(FIRST_USER_ID, userType);

		Mockito.when(userService.getUser(FIRST_USER_ID)).thenReturn(user);
		Mockito.when(userMapper.userToUserDTO(user)).thenReturn(userDTO);

		UserDTO result = userFacade.getUser(FIRST_USER_ID);

		Assertions.assertNotNull(result);
		Assertions.assertEquals(userDTO.id(), result.id());
		Assertions.assertEquals(userDTO.userName(), result.userName());
		Assertions.assertEquals(userDTO.email(), result.email());
		Assertions.assertEquals(userDTO.userType(), userType);

		Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
	}

	/**
	 * Test method for {@link UserFacadeImpl#getUser(long)}.
	 */
	@Test
	void getUser_notFound() {
		Mockito.when(userService.getUser(FIRST_USER_ID)).thenThrow(EntityNotFoundException.class);

		Assertions.assertThrows(EntityNotFoundException.class, () -> userFacade.getUser(FIRST_USER_ID));

		Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
	}

	/**
	 * Test method for {@link UserFacadeImpl#deleteUser(long)}.
	 */
	@Test
	void deleteUser() {
		userFacade.deleteUser(FIRST_USER_ID);    // deleteUser doesn't return anything yet, can't test success/failure

		Mockito.verify(userService, Mockito.times(1)).deleteUser(FIRST_USER_ID);
	}
}
