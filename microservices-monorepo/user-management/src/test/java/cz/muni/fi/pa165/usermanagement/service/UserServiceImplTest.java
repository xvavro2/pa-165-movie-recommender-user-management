package cz.muni.fi.pa165.usermanagement.service;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

	/**
	 * The ID of the first created user.
	 */
	public static final long FIRST_USER_ID = 1L;

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserServiceImpl userService;

	/**
	 * Test method for {@link UserServiceImpl#getAllUsers()}.
	 */
	@Test
	void getAllUsers() {
		List<User> users = TestDataFactory.createUserMultiple(5L);

		Mockito.when(userRepository.findAll()).thenReturn(users);

		List<User> result = userService.getAllUsers();

		Assertions.assertNotNull(result);
		Assertions.assertEquals(users.size(), result.size());
		for (int i = 0; i < users.size(); i++) {
			Assertions.assertEquals(users.get(i).getId(), result.get(i).getId());
			Assertions.assertEquals(users.get(i).getUserName(), result.get(i).getUserName());
			Assertions.assertEquals(users.get(i).getEmail(), result.get(i).getEmail());
			Assertions.assertEquals(users.get(i).getUserType(), result.get(i).getUserType());
		}
		Mockito.verify(userRepository, Mockito.times(1)).findAll();
	}

	/**
	 * Test method for {@link UserServiceImpl#createUser(User)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void createUser(final UserType userType) {
		User user = TestDataFactory.createUser(FIRST_USER_ID, userType);

		Mockito.when(userRepository.save(user)).thenReturn(user);

		User result = userService.createUser(user);

		Assertions.assertNotNull(result);
		Assertions.assertEquals(user.getId(), result.getId());
		Assertions.assertEquals(user.getUserName(), result.getUserName());
		Assertions.assertEquals(user.getEmail(), result.getEmail());
		Assertions.assertEquals(user.getUserType(), userType);
		Mockito.verify(userRepository, Mockito.times(1)).save(user);
	}

	/**
	 * Test method for {@link UserServiceImpl#getUser(long)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void getUser_success(final UserType userType) {
		User user = TestDataFactory.createUser(FIRST_USER_ID, userType);

		Mockito.when(userRepository.findById(FIRST_USER_ID)).thenReturn(Optional.of(user));

		User result = userService.getUser(FIRST_USER_ID);

		Assertions.assertNotNull(result);
		Assertions.assertEquals(user.getId(), result.getId());
		Assertions.assertEquals(user.getUserName(), result.getUserName());
		Assertions.assertEquals(user.getEmail(), result.getEmail());
		Assertions.assertEquals(user.getUserType(), userType);
		Mockito.verify(userRepository, Mockito.times(1)).findById(FIRST_USER_ID);
	}

	/**
	 * Test method for {@link UserServiceImpl#getUser(long)}.
	 */
	@Test
	void getUser_notFound() {
		Mockito.when(userRepository.findById(FIRST_USER_ID)).thenThrow(EntityNotFoundException.class);

		Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getUser(FIRST_USER_ID));
		Mockito.verify(userRepository, Mockito.times(1)).findById(FIRST_USER_ID);
	}

	/**
	 * Test method for {@link UserServiceImpl#updateUser(User)}.
	 */
	@Test
	void updateUser() {
		User user = TestDataFactory.createUser(FIRST_USER_ID, UserType.USER);

		Mockito.when(userRepository.save(user)).thenReturn(user);

		userService.updateUser(user);

		Mockito.verify(userRepository, Mockito.times(1)).save(user);
	}

	/**
	 * Test method for {@link UserServiceImpl#deleteUser(long)}.
	 */
	@Test
	void deleteUser() {
		userService.deleteUser(FIRST_USER_ID);    // deleteUser doesn't return anything yet, can't test success/failure

		Mockito.verify(userRepository, Mockito.times(1)).deleteById(FIRST_USER_ID);
	}
}
