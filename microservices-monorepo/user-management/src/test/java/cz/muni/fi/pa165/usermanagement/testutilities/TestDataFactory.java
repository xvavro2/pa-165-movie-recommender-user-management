package cz.muni.fi.pa165.usermanagement.testutilities;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"PMD", "checkstyle:hideutilityclassconstructor"})  // spring triggers linters false-positives
public class TestDataFactory {

	/**
	 * Creates a User entity with the given ID and UserType.
	 *
	 * @param id      The ID of the User entity.
	 * @param userType The UserType of the User entity.
	 * @return The created User entity.
	 */
	public static User createUser(final long id, final UserType userType) {
		User user = new User();
		user.setId(id);
		user.setUserName("testUser" + id);
		user.setEmail("test-email" + id + "@gmail.com");
		user.setEncryptedPassword("test-password" + id);
		user.setUserType(userType);
		return user;
	}

	/**
	 * Creates a list of User entities with the given count.
	 *
	 * @param count The number of User entities to create.
	 * @return The list of created User entities.
	 */
	public static List<User> createUserMultiple(final long count) {
		List<User> users = new ArrayList<>();
		for (long i = 1; i <= count; i++) {
			users.add(createUser(i, UserType.USER));
		}
		return users;
	}

	/**
	 * Creates a UserDTO with the given ID and UserType.
	 *
	 * @param id      The ID of the UserDTO.
	 * @param userType The UserType of the UserDTO.
	 * @return The created UserDTO.
	 */
	public static UserDTO createUserDTO(final long id, final UserType userType) {
		return new UserDTO(id, "testUser" + id, "test-email" + id + "@gmail.com", userType);
	}

	/**
	 * Creates a list of UserDTOs with the given count.
	 *
	 * @param count The number of UserDTOs to create.
	 * @return The list of created UserDTOs.
	 */
	public static List<UserDTO> createUserDTOMultiple(final long count) {
		List<UserDTO> usersDTO = new ArrayList<>();
		for (long i = 1; i <= count; i++) {
			usersDTO.add(createUserDTO(i, UserType.USER));
		}
		return usersDTO;
	}

	/**
	 * Creates a UserRegistrationDTO with the given identifier and UserType.
	 *
	 * @param identifier The identifier of the UserRegistrationDTO.
	 * @param userType   The UserType of the UserRegistrationDTO.
	 * @return The created UserRegistrationDTO.
	 */
	public static UserRegistrationDTO createUserRegistrationDTO(final String identifier, final UserType userType) {
		return new UserRegistrationDTO(
				identifier, identifier + "@gmail.com", identifier + "-password", userType
		);
	}

	/**
	 * Creates a UserUpdateDTO with the given identifier and UserType.
	 *
	 * @param identifier The identifier of the UserUpdateDTO.
	 * @param userType   The UserType of the UserUpdateDTO.
	 * @return The created UserUpdateDTO.
	 */
	public static UserUpdateDTO createUserUpdateDTO(final String identifier, final UserType userType) {
		return new UserUpdateDTO(
				identifier, identifier + "@gmail.com", identifier + "-password", userType
		);
	}
}
