package cz.muni.fi.pa165.usermanagement.service;

import cz.muni.fi.pa165.usermanagement.data.model.User;

import java.util.List;

/**
 * The UserService interface provides the blueprint for user management operations.
 * It outlines CRUD methods for user objects.
 */
public interface UserService {

    /**
     * Retrieves all users in the system.
     *
     * @return A list of User entities. If no users are found, this method returns an empty list.
     */
    List<User> getAllUsers();

    /**
     * Creates a new user in the system.
     *
     * @param user The User entity to be created.
     * @return The created User entity with an assigned ID.
     */
    User createUser(User user);

    /**
     * Updates an existing user in the system.
     * This method assumes that the user parameter contains an ID that identifies the user to be updated.
     *
     * @param user The User entity with updated information.
     */
    void updateUser(User user);

    /**
     * Retrieves a user by their ID.
     *
     * @param id The ID of the user to retrieve.
     * @return The User entity if found. Returns null if no user is found with the given ID.
     */
    User getUser(long id);

    /**
     * Deletes a user by their ID.
     *
     * @param id The ID of the user to delete.
     */
    void deleteUser(long id);
}
