package cz.muni.fi.pa165.usermanagement.facade;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.service.UserService;
import cz.muni.fi.pa165.usermanagement.util.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Primary
public class UserFacadeImpl implements UserFacade {

    private final UserMapper userMapper;
    private final UserService userService;

    /**
     * Constructs a UserFacadeImpl with specified UserService and UserMapper.
     *
     * @param userService the UserService to use for user operations.
     * @param userMapper  the UserMapper to use for converting between User and UserDTO objects.
     */
    @Autowired
    public UserFacadeImpl(final UserService userService,
                          final UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    /**
     * Fetches all users and converts them to DTOs.
     *
     * @return a List of UserDTO objects.
     */
    @Override
    @Transactional(readOnly = true)
    public List<UserDTO> getAllUsers() {
        return userMapper.usersToUserDTOs(userService.getAllUsers());
    }

    /**
     * Creates a new user from a UserRegistrationDTO.
     *
     * @param userDTO the UserRegistrationDTO containing user registration data.
     * @return a UserDTO representing the created user.
     */
    @Override
    @Transactional
    public UserDTO createUser(final UserRegistrationDTO userDTO) {
        User user = userMapper.userRegistrationDTOToUser(userDTO);
        return userMapper.userToUserDTO(userService.createUser(user));
    }

    /**
     * Updates a user identified by ID with new data from a UserUpdateDTO.
     *
     * @param id      the ID of the user to update.
     * @param userDTO the UserUpdateDTO containing updated user data.
     */
    @Override
    @Transactional
    public void updateUser(final long id, final UserUpdateDTO userDTO) {
        User existingUser = userService.getUser(id);
        userMapper.updateEntityWithDto(userDTO, existingUser);
        userService.updateUser(existingUser);
    }

    /**
     * Retrieves a user by ID and converts them to a UserDTO.
     *
     * @param id the ID of the user to retrieve.
     * @return a UserDTO representing the retrieved user.
     */
    @Override
    @Transactional(readOnly = true)
    public UserDTO getUser(final long id) {
        return userMapper.userToUserDTO(userService.getUser(id));
    }

    /**
     * Deletes a user identified by ID.
     *
     * @param id the ID of the user to delete.
     */
    @Override
    @Transactional
    public void deleteUser(final long id) {
        userService.deleteUser(id);
    }
}
