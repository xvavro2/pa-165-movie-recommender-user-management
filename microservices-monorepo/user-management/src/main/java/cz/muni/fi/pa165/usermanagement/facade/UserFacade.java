package cz.muni.fi.pa165.usermanagement.facade;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;

import java.util.List;

/**
 * The UserFacade interface provides a high-level abstraction for user management operations.
 * It offers methods to handle user data transfer objects (DTOs) for creating, updating, retrieving, and deleting users.
 */
public interface UserFacade {

    /**
     * Retrieves all users as DTOs.
     *
     * @return A list of UserDTO objects representing all users in the system.
     * If no users are present, an empty list is returned.
     */
    List<UserDTO> getAllUsers();

    /**
     * Creates a new user based on the provided UserRegistrationDTO.
     *
     * @param userDTO The UserRegistrationDTO containing the data necessary to create a new user.
     * @return The created UserDTO, representing the newly created user with its assigned ID and relevant data.
     */
    UserDTO createUser(UserRegistrationDTO userDTO);

    /**
     * Updates an existing user identified by the given ID with the data provided in the UserUpdateDTO.
     *
     * @param id      The ID of the user to be updated.
     * @param userDTO The UserUpdateDTO containing the updated user data.
     */
    void updateUser(long id, UserUpdateDTO userDTO);

    /**
     * Retrieves a single user by their ID, returning the data as a UserDTO.
     *
     * @param id The ID of the user to retrieve.
     * @return The UserDTO representing the retrieved user.
     */
    UserDTO getUser(long id);

    /**
     * Deletes a user identified by the given ID.
     *
     * @param id The ID of the user to delete.
     */
    void deleteUser(long id);
}
