package cz.muni.fi.pa165.usermanagement.data.repository;

import cz.muni.fi.pa165.usermanagement.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserRepository interface for handling persistence operations on User entities.
 * Extends JpaRepository to provide CRUD operations and JPA-specific functionality for User entities.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
